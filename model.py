import tensorflow as tf

from tensorflow.keras.layers import Input, Dense, Flatten, Conv2D, MaxPool2D, BatchNormalization, Dropout
from tensorflow.keras.models import load_model

from . import config
    
def calculate_iou(y_true, y_pred):
    def get_box(y):
        rows, cols, sizes = y[:, 0], y[:, 1], y[:, 2]
        rows, cols, sizes = rows * config.DIMENSION_Y, cols * config.DIMENSION_X, sizes * config.DIMENSION_Y
        y1, y2 = rows, rows + sizes
        x1, x2 = cols, cols + sizes
        return x1, y1, x2, y2

    def get_area(x1, y1, x2, y2):
        return tf.math.abs(x2 - x1) * tf.math.abs(y2 - y1)

    gt_x1, gt_y1, gt_x2, gt_y2 = get_box(y_true)
    p_x1, p_y1, p_x2, p_y2 = get_box(y_pred)

    i_x1 = tf.maximum(gt_x1, p_x1)
    i_y1 = tf.maximum(gt_y1, p_y1)
    i_x2 = tf.minimum(gt_x2, p_x2)
    i_y2 = tf.minimum(gt_y2, p_y2)

    i_area = tf.maximum(0., i_x2-i_x1) * tf.maximum(0., i_y2-i_y1)
    u_area = get_area(gt_x1, gt_y1, gt_x2, gt_y2) + get_area(p_x1, p_y1, p_x2, p_y2) - i_area

    return tf.math.divide(i_area, u_area)


class IoU(tf.keras.metrics.MeanSquaredError):
    def __init__(self, **kwargs):
        super(IoU, self).__init__(**kwargs)

        self.iou = self.add_weight(name='iou', initializer='zeros')
        self.total_iou = self.add_weight(name='total_iou', initializer='zeros')
        self.num_ex =  self.add_weight(name='num_ex', initializer='zeros')

    def update_state(self, y_true, y_pred, sample_weight=None):
        iou = calculate_iou(y_true, y_pred)
        self.num_ex.assign_add(1)
        self.total_iou.assign_add(tf.reduce_mean(iou))
        self.iou = tf.math.divide(self.total_iou, self.num_ex)

    def result(self):
        return self.iou

    def reset_state(self):
        self.iou = self.add_weight(name='iou', initializer='zeros')
        self.total_iou = self.add_weight(name='total_iou', initializer='zeros')
        self.num_ex = self.add_weight(name='num_ex', initializer='zeros')


def lr_schedule(epoch, lr):
    if (epoch + 1) % 5 == 0:
        lr *= 0.8
    return max(lr, 3e-7)


def create_keras_model():
    input_ = Input(shape=(config.DIMENSION_Y, config.DIMENSION_X, 1), name='image')

    x = input_

    for i in range(0, 5):
        n_filters = 2**(4 + 1)
        x = Conv2D(n_filters, 1, activation='relu')(x)
        x = BatchNormalization()(x)
        x = MaxPool2D(2)(x)

    x = Flatten()(x)
    x = Dense(256, activation='relu')(x)

    box_out = Dense(3, name='box_out')(x)

    keras_model = tf.keras.models.Model(input_, [box_out])

    keras_model.compile(
        loss={
            'box_out': 'mse'
        },
        optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),
        metrics={
            'box_out': IoU(name='iou')
        }
    )

    return keras_model


def load_keras_model(path):
    keras_model = load_model(path, custom_objects={"IoU":IoU})
    return keras_model
