import os

DIMENSION_X = 144
DIMENSION_Y = 108

BUCKET_NAME = 'training_target'
GCS_BUCKET = f"gs://{BUCKET_NAME}"
SAVE_PATH = os.path.join(GCS_BUCKET, "model")
