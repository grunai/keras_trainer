import os
import random
import numpy as np
import urllib.request
import tarfile
import matplotlib.pyplot as plt

from PIL import Image, ImageDraw

from . import config

url = "http://dl.dropboxusercontent.com/s/5m7t360dpc66whr/samples.tar.gz"
samples_file = "samples.tar.gz"
urllib.request.urlretrieve(url, samples_file)
with tarfile.open(samples_file) as samples:
  samples.extractall('./')

neg_files = os.listdir('neg')

target_image = Image.open(os.path.join('pos', 'target.png')).convert('RGBA')

def create_example():
    size = np.random.randint(int(config.DIMENSION_Y/4), config.DIMENSION_Y)
    imagename = random.choice(neg_files)
    image_path = os.path.join('neg/', imagename)
    image = Image.open(image_path).convert('RGB')
    image.load()
    image = image.resize((config.DIMENSION_X, config.DIMENSION_Y))
    row = np.random.randint(0, config.DIMENSION_Y - size)
    col = np.random.randint(0, config.DIMENSION_X - size)
    resized_target = target_image.copy().resize((size, size))
    image.paste(resized_target, (col, row), mask=resized_target)
    image = image.convert('L')
    image = np.asarray(image).copy()
    return image.astype('uint8'), row/config.DIMENSION_Y, col/config.DIMENSION_X, size/config.DIMENSION_Y


def data_generator(batch_size=16):
    while True:
        x_batch = np.zeros((batch_size, config.DIMENSION_Y, config.DIMENSION_X))
        bbox_batch = np.zeros((batch_size, 3))

        for i in range(0, batch_size):
            image, row, col, size = create_example()
            x_batch[i] = image / 255.
            bbox_batch[i] = np.array([row, col, size])

        yield x_batch, bbox_batch


def plot_bounding_box(image, gt_coords, pred_coords=[], norm=False):
    if norm:
        image *= 255.
        image = image.astype('uint8')
    image = Image.fromarray(image)
    image = image.convert('RGB')
    draw = ImageDraw.Draw(image)

    row, col, size = gt_coords
    row *= config.DIMENSION_Y
    col *= config.DIMENSION_X
    size *= config.DIMENSION_Y
    draw.rectangle((col, row, col + size, row + size), outline='green', width=3)

    if len(pred_coords) == 3:
        row, col, size = pred_coords
        row *= config.DIMENSION_Y
        col *= config.DIMENSION_X
        size *= config.DIMENSION_Y
        draw.rectangle((col, row, col + size, row + size), outline='red', width=3)

    return image


def test_model(model, test_datagen):
    x, box = next(test_datagen)

    pred_box = model.predict(x)

    pred_coords = pred_box[0]
    gt_coords = box[0]
    image = x[0]

    image = plot_bounding_box(image, gt_coords, pred_coords, norm=True)

    plt.imshow(image)
    plt.xticks([])
    plt.yticks([])


def test(model):
    test_datagen = data_generator()

    plt.figure(figsize=(16, 4))

    for i in range(0, 15):
        plt.subplot(3, 5, i + 1)
        test_model(model, test_datagen)
    plt.show()